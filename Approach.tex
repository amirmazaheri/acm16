% !TEX root = main.tex
\section{Proposed approach}
\label{sApproach}
We introduce our main approach to multi-concept based video retrieval in this section. We firstly formalize a retrieval model and then describe how to integrate different types of concept detectors into the model. 


\subsection{Retrieval model} \label{sModel}
Ranking videos according to one or more concepts selected by the users/systems is the central component in modern video retrieval systems (cf.\ Figure~\ref{fConcept}). Whereas the existing works often simply weighted-average the concepts' detection confidences to rank the videos, we aim to enhance this component by a principled retrieval model, which is flexible enough to incorporate different kinds of concept detectors.

Denote by $\calQ$ all the concepts offered by the system for the users to compose queries, by $\calV$ all the videos in the the database, and by $R(Q)\subset\calV$ the videos that are related to a multi-concept query $Q\subset\calQ$. Accordingly, the retrieval model should possess some mechanism to differentiate the groundtruth subset $R(Q)$ from any other subsets of the videos in the database and tell that
\begin{align}
\forall S\subset\calV, S\neq R(Q),\; R(Q) \text{ is a better output than } S,  \label{eRetrieval}
\end{align}
given the querying concepts in $Q$. Directly modeling this notion gives rise to a structured prediction model presented in~\cite{siddiquie2011image} and strengthened in \cite{yu2012weak}. We appreciate that this is perhaps the most natural choice for the retrieval model. Unfortunately, it suffers from high computation costs due to the  exponential number $2^{|\calV|}$ of distinct subsets of $\calV$. Moreover, the expressiveness of the model's scoring function is limited to special forms, to tailor the function to utilize the training and testing algorithms for structured prediction~\cite{tsochantaridis2004support,petterson2010reverse}. Our own experiments tell that it is computationally intractable to use this retrieval model for the shot-level concept detections (cf.\ Section~\ref{sShot}).

\subsubsection{Retrieval as ranking} Instead, we relax the retrieval to a ranking problem as in~\cite{grangier2008discriminative}  and managed to accommodate multiple latent variables to tackle the shot-level detections. In particular, the rigorous criterion (eq.~(\ref{eRetrieval})) for retrieval is replaced by a less constrained ranking criterion,
\begin{align}
\forall V_i \in R(Q), \forall V_j\notin R(Q), \; V_i \text{ ranks ahead of } V_j, \label{eRanking}
\end{align}
where $V_i$ and $V_j$ are a pair of videos in the database $\calV$. 

Comparing eq.~(\ref{eRetrieval}) with eq.~(\ref{eRanking}), the former calls for a model to operate over $2^{|\calV|}$ subsets of videos while for the latter we only need a model to assign a ranking score for each video $V\in\calV$. We use the following ranking model in this work $\calF:  {\calQ}\times \calV\mapsto \R$,
\begin{align}
\calF(Q,V) = \frac{1}{|Q|}\sum_{q\in Q} f(q,V|\mat{\Theta}), \label{eScore}
\end{align}
which breaks down into several ranking scoring functions $f(q,V|\mat{\Theta}), q\in Q,$ each for an individual concept, and $\mat{\Theta}$ denotes the model parameters. We shall write $f(q)\triangleq f(q,V|\mat{\Theta})$ in the following for brevity, and leave the discussion of the scoring functions to Sections~\ref{sVideo}~and~\ref{sShot}.

Given a multi-concept query $Q$ provided  by a user or by the system, we simply rank the videos in the database by $\calF$ and return the top portion of the ranking list to the user. Compared to the retrieval model based on structured prediction~\cite{siddiquie2011image,yu2012weak}, our model is not able to optimize the number of videos to output. However, we argue that this does not reduce the usability of our ranking model, considering that common users are used to ranking lists due to text retrieval. 
 
\subsubsection{Learning model parameters $\mat{\Theta}$} 
In order to train the model, we employ the strategy used in ranking SVM~\cite{joachims2002optimizing,herbrich1999large} and arrive at the following:
\begin{align}
\min_{\mat{\Theta}} \quad \sum_Q \frac{1}{\left|\calN(Q)\right|}\sum_{(i,j)\in \calN(Q)} L\left(\calF(Q,V_j) - \calF(Q,V_i)\right), \label{eP}
\end{align}
where $\calN(Q)$ is the collection of all the pairs of videos $V_i$ and $V_j$ in eq.~(\ref{eRanking}) for the query $Q$, and $L(x)\geq 0$ is a loss function. The loss function will impose some amount of penalty  when the ranking scores of a pair of videos violate the ranking constraint of eq.~(\ref{eRanking}). 

We exploit two types of loss functions in this work, the hinge loss $L_{\text{hinge}}(x)=\max(x+1,0)$ and 0-1 loss $L_{0\text{-}1}(x)$ which takes the value $1$ when $x>0$ and 0 otherwise. \textit{Note that we cannot actually solve the optimization problem with the 0-1 loss;} we instead use it to define a novel early stopping criterion when we solve the problem with hinge loss by sub-gradient descent. Namely, the program stops when the change of the objective function value, computed from the 0-1 loss, is less than a threshold ($10^{-10}$ in our experiments). 

As a result, we are able to take advantage of the fact that the 0-1 loss is more robust than the hinge loss when there are outliers in the data. The hinge loss alone would be misled by the outliers and results in solutions that are tuned away from the optimum, while the 0-1 loss helps avoid that situation by suppressing the penalties incurred by the outliers. Indeed, the novel stopping criterion by the 0-1 loss significantly improves the results of hinge loss in our experiments. 


Note that the 0-1 loss based stopping criterion is another key point clearly differentiating our approach from~\cite{grangier2008discriminative}, which motivates our ranking model. In addition, we introduce a family of new scoring functions for different concept detections, especially the one with latent variables  in Section~\ref{sShot}. 


\subsection{Video-level concept detection} \label{sVideo}
To quickly respond to the users\rq{} queries, it is often the case that the concept detection results $\vct{\phi}(V)$ over each video $V\in\calV$ are computed off-line and then stored somewhere. We use $\vct{\phi}$ as the shorthand of $\vct{\phi}(V)$. Note that $\vct{\phi}$ is a $|\calQ|$-dimensional vector whose entry $\phi_q$ corresponds to the detection confidence of the concept $q$ (in a video $V$). We next describe how to use this vector, the video-level concept detection results, to design the scoring functions $f(q), q\in Q\subset\calQ$. We start from the weighted average which prevails in the existing video retrieval works. 

\subsubsection{Weighted average} Recall that the overall scoring function $\calF(Q,V)$ breaks down into several individual functions $f(q,V|\mat{\Theta})\triangleq f(q), q\in Q$, each of which accounts for one concept (eq.~(\ref{eScore})). A common practice to rank the videos given a multi-concept query $Q$, is  by the weighted average of  the corresponding concept detection confidences:
\begin{align}
f^{\text{V}}_{\text{avg}}(q) = \mat{\Theta}_{qq} \,\phi_q \triangleq \inner{\vct{1}^{q}}{\vct{\phi}}, \label{eAvg}
\end{align}
where the weights $\mat{\Theta}_{qq}, q\in Q$ could be the similarities between the concepts in $Q$ and an open-vocabulary user query~\cite{wang2007importance,kennedy2008query}. We only study the uniform weights $\mat{\Theta}_{qq}=1$ in this work without loss of generality. 

Note that this weighted average fails to model either 1) the correlations between the concepts in $Q$ or  2) the correlations between $Q$ and the remaining unselected concepts in $\calQ$. To see this point more clearly, we denote by $\vct{1}^{q}\in\R^{|\calQ|}$ the one-hot vector taking the value 1 at the $q$-th entry and zeros else. The rightmost of eq.~(\ref{eAvg}) thus follows. Further, the model parameters $\mat{\Theta} = (\vct{1}^{1}, \vct{1}^{2},\cdots,\vct{1}^{|\calQ|})^T=I \in \R^{|\calQ|\times |\calQ|}$ actually correspond to an identity matrix. The entry $\mTheta_{qp}$, which is supposed to encode the relationship of concepts $q$ and $p$, is 0 in the weighted average (eq.~(\ref{eAvg})).

\subsubsection{Encoding concept correlations} To this end, the natural extensions to the weighted average scoring function are the following,
\begin{align}
f^{\text{V}}_{\text{corr-1}}(q) &= \inner{\vtheta^q}{\vphi},\; \text{such that } \;\vtheta_{p}^q= 0 \; \text{if } p \notin Q, \\
f^{\text{V}}_{\text{corr-2}}(q) &= \inner{\vtheta^q}{\vphi},\; \text{such that }\; \vtheta^q \in \R^{|\calQ|},  \label{eCorr2}
\end{align}
where $f^{\text{V}}_{\text{corr-1}}(q)$ considers the contributions from the other concepts $p\in Q$ when it scores the concept $q$ for a video $V$. Indeed, the existence of ``computer'' could affect the confidence of ``furniture'' when both are selected to the query $Q\subset \calQ$. The other function $f^{\text{V}}_{\text{corr-2}}(q)$ further considers all the other concepts, when it scores for instance the concept $q=$``furniture'' in a video, capturing the case that the lack of ``beach'' may reinforce the confidence about ``furniture''. Note that the  parameters $\mTheta= (\vct{\theta}^{1}, \cdots,\vct{\theta}^{|\calQ|})^T$ become a full matrix now, offering more modeling flexibilities. 

\subsection{Shot-level concept detection} \label{sShot}
In practice many concept detectors actually take the video shots, or even frames, as the input~\cite{ye2015eventnet,sadanand2012action,althoff2012detection}. Suppose for a video $V$ in the database we have partitioned it into $\cst{H}$ shots. A video retrieval system  can then pre-compute and store the concept detection results $\vphi^h\in\R^{|\calQ|}, h=1,\cdots,\cst{H}$ for all the concepts $\calQ$ over the shots of the video. Compared to the video-level concept detections, the shot-level detections provide more insights and finer-grained information about the video database. We thus propose some new forms of scoring functions to take advantage of such detection results.

\subsubsection{Scoring the best shot for the querying concepts} One potential benefit we can have from the shot-level concept detections is that, among all the shots of a video $V$, we can select the most informative shot for the scoring function:
\begin{align}
f^{\text{S}}_{\text{latent}}(q) &= \max_{h\in\{1,2,\cdots,\cst{H}\}} \inner{\vtheta^q}{\vphi^h},  
\end{align}
where the model parameters $\vtheta^q \in \R^{|\calQ|}$, which correspond to the concept $q\in Q\subset\calQ$, count the contributions to $q$ from all the concepts within the shot, which will be selected by the latent variable $h\in\{1,2,\cdots,\cst{H}\}$. 

We argue that this formation can better model the \emph{negative} correlations between concepts, if any, than the $f^{\text{V}}_{\text{corr-2}}(q)$ defined over the video level. Indeed, consider a set of negatively correlated concepts (e.g., ``beach'', ``furniture'', etc.). They could all have very strong responses across a short video. For instance, a tourist may capture a video within a hotel room and then shift to the beach outside. As a result, both ``beach'' and ``furniture'' will be detected with high confidences in the video but they are exclusive over a single shot. As a result, the video-level scoring function may be confused by this video but $f^{\text{S}}_{\text{latent}}(q)$ scores a single best shot and is thus robust to this scenario.

\subsubsection{Scoring best shot for each querying concept} 
While $f^{\text{S}}_{\text{latent}}(q)$ well captures the negative concept correlations, it may be inadequate to track the \emph{positive} correlations between different concepts by focusing on only a short shot; very few concepts could appear simultaneously in a single shot. We thus compensate it by a second term:
\begin{align}
f^{\text{VS}}_{\text{latent}}(q) &= \max_{h\in\{1,\cdots,\cst{H}\}} \inner{\vtheta^q}{\vphi^h} +  \sum_{p\in\calQ} \max_{g\in\{1\cdots,\cst{H}\}}{{\upsilon}^{q}_p}{\phi^{g}_p},  \label{eVS}
\end{align}
where $\max_{g}{{\upsilon}^{q}_p}{\phi^{g}_p}$ max-pools the confidences of each concept across all the shots of video $V$.  Note that we therefore provide two complementary types of modeling capabilities in $f^{\text{VS}}_{\text{latent}}(q)$. The first term is robust to the concepts which are negatively correlated with $q$ and the second terms strengthen the detection score of concept $q$ from some positively correlated concepts in the video. The model parameters $\vtheta^q$ and $\vct{\upsilon}^q$ are learned by solving eq.~(\ref{eP}) with sub-gradient descent. Details are shown as follow.

\subsection{Optimization}
We solve for the model parameters by (sub-)gradient descent. As discussed in Section 3.1, the loss function $L$ is non-zero on the pairs $\{(V_i,V_j)\}$, for each of which the negative video $V_j$ has higher ranking score than the positive $V_i$. As a result, we get the gradients on those pairs and for the other pairs the gradients are simply zero.

Denoting by
\begin{align}
	\calS_j = \frac{\partial L}{\partial \calF(Q,V_j)}\times\frac{\partial \calF(Q,V_j)}{\partial \mat{\Theta}},
\end{align}
we thus have the overall gradients of eq.~(\ref{eP}) by
\begin{align}
	%\frac{\partial M}{\partial \Theta} =
	\quad \sum_Q \frac{1}{\left|\calN(Q)\right|}\sum_{(i,j)\in \calN(Q)}\left( \calS_j - \calS_i \right). \label{eD}
\end{align}



Note that the model parameters $\mat{\Theta}$ consist of two parts $\left(\vtheta,\vct{\upsilon}\right)$, corresponding to the two terms of $f^{\text{VS}}_{\text{latent}}$ (cf.\ eq.~(\ref{eVS})), respectively. We compute the gradients with respect to the first part $\vtheta$ using the $softmax$ derivation to approximate a smooth gradients, as suggested by~\cite{ping2014marginal}:
\begin{align}
	\frac{\partial \calF(Q,V_j)}{\partial \vtheta}=\sum_{h\in\{1,2,\cdots,\cst{H}\}} \dfrac{e^{\inner{\vtheta^q}{\vphi^h}}\vphi^h}{\sum_{j\in\{1,2,\cdots,\cst{H}\}}e^{\inner{\vtheta^q}{\vphi^j}}}.
\end{align}

We write out the gradients with respect to the second part $\vct{\upsilon}$ over different dimensions of $\vct{\upsilon}$. Recall that he second term of $f^{\text{VS}}_{\text{latent}}$ (cf.\ eq.~(\ref{eVS})), $\max_{g}{{\upsilon}^{q}_p}{\phi^{g}_p}$, max-pools over all the shots of a video for each single concept. As a result, we have the following:
\begin{align}
	\frac{\partial \calF}{{\upsilon}^{q}_{p}} =  \phi^{g*}_{p},	\quad p=1,2,\cdots,\cH, \;q=1,\cdots,\cH
\end{align}
where ${g*}$ is determined by $g*\leftarrow \max_{g}{{\upsilon}^{q}_{p}}{\phi^{g}_{p}}$.

