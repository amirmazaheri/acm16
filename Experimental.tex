% !TEX root = main.tex
\section{Experimental Results} \label{sExp}
This section presents our experiments to evaluate the proposed multi-concept video retrieval model along with the various scoring functions. We firstly describe the experiment setup and then report the retrieval results. We further give some detailed analyses and qualitative results. % some exemplar videos to qualitatively analyze the effects of different scoring functions.


\subsection{Experiment setup}
Our experiments depend on two separate datasets respectively for video retrieval and training the concept detectors. We further exploit four sets of multi-concept queries. Two sets consists of 50 queries in the form of concept pairs, one for training and testing and the other just for testing. Two other sets contain 50 triplets and 30 single concept queries to be used just in testing.  We train our model using only the first set of queries on the training set, and then test it by all four sets of queries on the test set. 



\begin{table*}
\centering
\caption{\normalsize{The multi-concept queries used in our experiments.} }   \label{tQueries}
\begin{small}
\begin{tabular}{|cl|}
    \hline
    & \multicolumn{1}{c|}{\normalsize{Queries}} \\
    \hline
     &  (Chair, Computers), (Boat/Ship, Oceans),  (Classroom, Computers), (Instrumental\_Musician, Singing ), (Chair, Classroom)\\

     & (Boat/Ship, Running), (Chair, Nighttime), (Boat/Ship, Quadruped), (Bicycling, Forest), (Chair, Hand), (Chair, Flags)\\
     & (Boat/Ship, Forest), (Nighttime, Singing ), (Cheering, Singing ), (Forest, Lakes), (Chair, Telephones), (Running, Stadium)\\
       50&  (Chair, Forest), (Beach, Boat/Ship), (Oceans, Quadruped ), (Forest, Quadruped ), (Beach, Quadruped), (Running, Forest)\\ 
        Concept & (Bridges, Forest), (Boat/Ship, Bridges),  (Instrumental\_Musician, Nighttime), (Highway, Nighttime), (Beach, Oceans)\\
     Pairs &  (Bus, Highway), (Bus, Chair), (Nighttime, Forest), (Highway, Forest), (Computers, Telephones), (Nighttime, Running)\\
      &  (Bridges, Highway), (Cheering, Flags), (Cheering, Instrumental\_Musician), (Cheering, Nighttime), (Forest, Oceans)\\
      &  (Chair, Highway), (Chair, Quadruped ), (Boat/Ship, Lakes), (Running, Quadruped), (Nighttime, Flags), (Bridges, Chair)\\
 & (Boat/Ship, Nighttime), (Demonstration\_Or\_Protest, Flags), (Airplane, Boat/Ship), (Boat/Ship, Chair), (Chair, Running)\\
    \hline

\end{tabular}
\end{small}
\vspace{-10pt}
\end{table*}


\subsubsection{The IACC.2.B dataset for video retrieval} 
We mainly test our approach over the IACC.2.B dataset which is the test set used in the Semantic Indexing (SIN) task of TRECVID 2014~\cite{over2014trecvid}. The dataset comprises 2,371 Internet videos which are ``characterized by a high degree of diversity in creator, content, style, production qualities, original collection device/encoding, language, etc.''~\cite{over2014trecvid}. The video durations range from 10 seconds to 6.4 minutes with the mean of 5 minutes. Standard shot partitions (106,000 shots in total) are provided by the dataset and 30 concepts are annotated for the shots. We also test our main approach on IACC.2.C dataset which is the test set for SIN task of TRECVID 2015 ~\cite{2015trecvidover}, with similar settings, as a correctness proof of our approach.

We randomly split IACC.2.B into a training set of 712 videos, a validation set of 474 videos, and a test set of 1,185 videos. From the $\binom{30}{2}$ possible pairs of concepts, we select 50 pairs as the first set of queries in our experiments. For each query, we consider that a video is related when each concept in the query has at least one positive shot in the video. This results in  minimally {27}, maximally {86}, and on average {44} out of the 1,185 videos in the database (i.e., the test set) related to the concept-pair queries. Additionally, we also build the second set of queries with {50} concept triples. There are on average {24} related videos to a concept-triplet query. Note that the more concepts a query comprises, the more challenging the retrieval task is due to the smaller number of related videos in the database. See Table~\ref{tQueries} for some queries. Pair queries in this table are the only ones used to train a model. 
%Other queries, which are used just for testing are included in supplementary material.

\subsubsection{Evaluation } 
We use one of the most popular  metrics in information retrieval, normalized discounted cumulative gain (NDCG)~\cite{jarvelin2008discounted}, to evaluate the ranking lists returned by our model in response to the multi-concept queries. Given a ranking list for query $Q$, NDCG is calculated by:
\begin{align} 
\text{NDCG}@k = \frac{1}{Z}\sum\limits_{j=1}^{k}\frac{{G}[j]}{1 + \log_{2} j}, \label{eNDCG}
\end{align}
where $j$ is the rank of a video and ${G}[j] = \text{rel}(j)^2$ with $\text{rel}(j)$ being the number of positive concepts shared by that video and the query $Q$. The partition $Z$ is  calculated from the ideal ranking list such that any NDCG$@k$ value is normalized between 0 and 1. We shall report the results  at $k=5,10,\cdots,50$ in the following experiments.



\subsubsection{Concept detectors} \label{sDetector}
It has been an active research area to learn robust concept detectors for videos~\cite{qi2007correlative,ye2015eventnet,snoek2013mediamill}. Virtually all kinds of concept detectors can be employed in our retrieval model, as long as they output the shot or video level detection confidences. We cannot include and analyze all the concept detectors developed in the literature. Instead, we train our own detectors following the practice of~\cite{dehghanucf}.

In particular, we train 60 independent detectors from the training data (IACC.1.tv10.training, IACC.1.A, IACC.1.B, and IACC.1.C)  of the TRECVID 2014 SIN task~\cite{over2014trecvid} for the concepts with key frame annotations, including the 30 concepts annotated in IACC.2.B. To this end, we extract dense SIFT~\cite{lowe2004distinctive} (DSIFT) and Convloutional Neural Network (CNN) features \cite{krizhevsky2012imagenet} from the annotated (both positive and negative) key frames for the 60 concepts. The DSIFT features coming from VLFeat~\cite{vedaldi2010vlfeat} toolbox, are encoded by Fisher vectors~\cite{perronnin2010improving} as an image representation and then input to linear SVMs. For CNN features, we use the  activations of ``relu6'' and ``fc7'' layers as two types of image representations, train SVMs with histogram intersection kernels for either of them, and then average the detection scores of the two types of SVMs. Overall, we thus harvest two complementary detection confidences for any concept, one from the DSIFT and the other from the CNN features. They are both transformed to probabilities using the Platt calibration. At the testing stage, we firstly average them to obtain the concept detection confidences for each frame, then max-pool the scores within a shot to have the shot-level results $\vphi^h$, and finally arrive at the video-level concept detection results $\vphi$ again by  max-pooling.



\subsubsection{Practical considerations in implementation} 
In our implementation, we add $\sum_{q\in\calQ}\lambda\twonorm{\vtheta^q} + \gamma\twonorm{\vct{\upsilon^q}}$ to regularize the optimization problem in eq.~(\ref{eP}) and tune the hyper-parameters $\lambda$ and $\gamma$ using the validation set. Note that $\gamma=0$ except for the scoring function $f^{\text{VS}}_{\text{latent}}(q)$. For the shot-level concept detections, we impose symmetric constraints over the model parameters (i.e., $\vtheta^{q}_p=\vtheta^{p}_q$). When we train the model with latent variable $h$ (cf.\ $f^{\text{S}}_{\text{latent}}$ and $f^{\text{VS}}_{\text{latent}}$), we remove the shots without annotations from negative training videos. Both $\vtheta^q$ and $\vct{\upsilon}^q$ are initialized by one-shot vectors. %The latent variable $h$ results in a non-convex optimization and the gradient descent may only find a local optimum. In this case, we initialize the model parameters $\vtheta^q$ by the one-hot vector used in the baseline $f^{\text{V}}_{\text{avg}}$ and set $\vct{\upsilon}^q =\zeros$.





\begin{table*}
\centering
\caption{Comparison results of different scoring functions in pair-concept based video retrieval.}   \label{tComparison}
\begin{small}
\begin{tabular}{|lr|cccccccccc|c|}
	\multicolumn{13}{l}{ \textbf{Baselines}}\\
	\hline
	\multicolumn{2}{|c|}{Functions} & NDCG$@5$ & $@10$ & $@15$ & $@20$ & $@25$ & $@30$ & $@35$ & $@40$ & $@45$ & $@50$ & Mean\\
	\hline
	Common practice & $f^{\text{V}}_{\text{avg}}$ & 0.626 &	0.571 &	0.556 &	0.561	& 0.575 & 	0.588 &	0.597 &	0.610 &	0.620 &	0.626 &	0.593 \\
	\hline
	TagProp~\cite{guillaumin2009tagprop} &   & 0.300&	0.273&	0.256&	0.268&	0.277&	0.286&	0.294&	0.301&	0.308&	0.314&	0.288 \\
	\hline
	Rank-SVM~\cite{chapelle2010efficient} &   & 0.579&	0.529&	0.522&	0.526&	0.543&	0.554&	0.565&	0.568&	0.577&	0.581&	0.555 \\
	\hline
	Co-occurrence ~\cite{assari2014video} &   & 0.594&	0.507&	0.486&	0.495&	0.518&	0.534&	0.549&	0.556&	0.564&	0.574&	0.538 \\
	\hline
	PicSOM 2013~\cite{ishikawa2013picsom} &   & 0.630 &	0.571 &	0.555 &	0.559	& 0.573 & 	0.581 &	0.592 &	0.605 &	0.615 &	0.621 &	0.590 \\
	\hline
    \multicolumn{13}{l}{$|\calQ|\mathbf{=30}$ \textbf{concepts}}\\
    \hline
    Video-level &$f^{\text{V}}_{\text{corr-1}}$ & 0.616&	0.570&	0.555&	0.558&	0.570&	0.581&	0.595&	0.605&	0.615&	0.622&	0.589 \\
    \hline
    Video-level &$f^{\text{V}}_{\text{corr-2}}$ & 0.648	& 0.592 &	0.574 &	0.576 &	0.589 &	0.603 &	0.615 &	0.623 &	0.631	& 0.633 & 	0.609 \\
\hline
    Shot-level  &$f^{\text{S}}_{\text{latent}}$ & \textit{0.682}		& \textit{0.610}	&\textit{0.595}&	0.595&	0.612&	0.621&	0.636&	0.649&	0.658&	0.661&	\textit{0.632}\\
\hline
    Shot-level & $f^{\text{VS}}_{\text{latent}}$ & 0.629 &	0.588 &	0.583 &	\textit{0.600} &	\textit{0.618} &	\textit{0.631} &	\textit{0.647} &	\textit{0.654} &	\textit{0.662} &	\textit{0.671} &	0.628\\
\hline 
    \multicolumn{13}{l}{$|\calQ|\mathbf{=60}$ \textbf{concepts}}\\
    \hline 
    
       Videl-level & $f^{\text{V}}_{\text{corr-1}}$ & 0.616&	0.570&	0.555&	0.558&	0.570&	0.581&	0.595&	0.605&	0.615&	0.622&	0.589 \\
    \hline
    Video-level & $f^{\text{V}}_{\text{corr-2}}$ &  0.648 &	0.588 &	0.573 &	0.573 &	0.588 &	0.600	&0.611	&0.624	&0.629	&0.632	&0.607 \\
    \hline
      Shot-level & $f^{\text{S}}_{\text{latent}}$ & 0.676	& 0.618 &	 0.598	& 0.597 &	 0.615 &	0.626	& 0.641 &	0.651 &	0.658 &	0.665 &	0.635 \\
      \hline
            Shot-level & $f^{\text{VS}}_{\text{latent}}$ & \textbf{0.698}	& \textbf{0.638}	& \textbf{0.617}	& \textbf{0.609} & 	\textbf{0.630}&	\textbf{0.641}	&\textbf{0.654}	&\textbf{0.664}	&\textbf{0.668}	&\textbf{0.674} &	\textbf{0.649} \\
            \hline
\end{tabular}
\end{small}
%\vspace{-10pt}
\end{table*}





\subsection{Comparison results}
In Table~\ref{tComparison}, we report the comparison results of different scoring functions that account for different types of concept detectors. All the 50 pair-concept queries (cf.\ Table~\ref{tQueries}) are tested and evaluated using NDCG$@k$ in turn. The last column is the mean of its left columns, i.e., the mean of the NDCG values at $k=5,10,\cdots,50$.

Following the common practice in the existing concept-based video retrieval systems, we empirically test a variety of fusion methods~\cite{campbell2007ibm,yan2003combination,ishikawa2013picsom} as the (old) baselines---our approach offers a new set of simple yet more advanced baselines for the concept-based video retrieval. Probably because our detectors output probabilities after the Platt calibration, the average operation in $f^{\text{V}}_\text{avg}$ performs the best among the fusion techniques discussed in~\cite{campbell2007ibm}. We thus only show 
the results of $f{\text{V}}_\text{avg}$ and the second best, PicSOM~\cite{ishikawa2013picsom},  in the rows tagged by ``Common practice'' and ``PicSOM 2013'', respectively,  in Table~\ref{tComparison}. The PicSOM fusion strategy~\cite{ishikawa2013picsom} involves a convex combination of the product and the average of the querying concepts' detection scores. Also, we used a another common technique as explained in ~\cite{assari2014video} to capture just positive correlation between pairs of concepts, using a Co-occurrence matrix of them built in training stage.

We further include ranking SVM~\cite{joachims2002optimizing} and TagProp~\cite{guillaumin2009tagprop} in the table. Both takes as input the video-level representations; they are not able to handle the set of shot-level features in each video. We use two types of inputs, the video-level concept detection scores and CNN features as the video representations to train the TagProp and ranking SVM models. Note that we train a ranking SVM model for each of the pair-concept queries. TagProp is a state-of-the-art image tagging algorithm.  It uses K-nearest neighbor and metric learning to propagate the tags of training examples to any testing instance. We reported the best results for each after parameter tuning on our validation set. Probably because our training set is relatively small, the TagProp results are very bad. Ranking SVM gives rise to comparable results with the other fusion techniques. 




There are four types of scoring functions in our approach, $f^{\text{V}}_{\text{corr-1}}$ and $f^{\text{V}}_{\text{corr-2}}$ accounting for the video-level concept detections and $f^{\text{S}}_{\text{latent}}$ and $f^{\text{VS}}_{\text{latent}}$ for the shot-level concept detections. We learn their model parameters by the 0-1 loss and the 50 pair-concept queries (cf.\ Table~\ref{tQueries}) using the videos in the training set. Experiments comparing the hinge loss and the 0-1 loss are presented in Section~\ref{sExpLoss}. 
\vspace{-5pt}

\paragraph{Comparison} 
There are $|\calQ|=30$ concepts labeled for our video database $\calV$, which is drawn from the IACC.2.B dataset. All our queries are constructed from these concepts such that we have the groundtruth ranking list for evaluation. We first see the video retrieval results in the top half of Table~\ref{tComparison}. The variations of our model with different scoring functions all improve the common practice $f^{\text{V}}_{\text{avg}}$. The margins between $f^{\text{V}}_\text{avg}$ and our latent shot-level functions are especially significant. 
%Moreover, the $f_\text{shot-VS}$ outperforms $f_\text{shot-S}$ due to that it is able to take advantage of both within-shot and inter-shot inter-dependencies of different concepts.
\vspace{-5pt}


\begin{figure}[h]
\centering
  \includegraphics[scale=0.4]{./images/bg-HingeVsBinaryLoss}
  \caption{The effects of the 0-1 loss based stopping criterion in optimization. For both the video-level scoring function $f^{\text{V}}_\text{corr-2}$ and shot-level function $f^{\text{VS}}_{\text{latent}}$, the introduction of the 0-1 loss significantly improves the performance of the hinge loss.}
  \label{fHinge-0-1}
  \vspace{-20pt}
\end{figure}




\paragraph{The benefit of more concepts} Though the queries are built from the vocabulary of 30 concepts, we are actually able to harvest more concept detectors from another independent dataset, TRECVID 2014 SIN task training set. Our model is flexible to include them by expanding the concept detection vectors $\vphi$ (see Section~\ref{sApproach}). The bottom half of Table~\ref{tComparison} shows the results corresponding to 60 concept detectors. We see that the observations about the relative performances of the model variations from the $|\calQ|=30$ concepts still hold. In addition, the video retrieval results using the shot-level scoring functions have been significantly improved over those of the 30 concepts. This is in accordance with our intuition as well as the observation in~\cite{yu2012weak}. Indeed, the inter-dependences of more concepts may provide better cues for our scoring functions and make them more robust to the unreliable concept detection confidences. 

Note that, however, introducing more concepts does not change the results of the video-level scoring function $f^{\text{V}}_\text{corr-2}$ too much, $f^{\text{V}}_\text{corr-1}$ is not affected by extra concepts. We argue that this is mainly due to the fact that our detectors are not developed for the video-level concept detections. For the future work, it is thus interesting to see whether more video-level concept detectors, such as the action classifiers~\cite{wang2013action,hou2014damn}, can benefit our  video-level function $f^{\text{V}}_\text{corr-2}$ as well. Another interesting direction  would be to pursue the weak attributes/concepts in the video retrieval task~\cite{yu2012weak}. 




\subsection{The effect of the 0-1 loss} \label{sExpLoss}
We study the effect of the novel 0-1 loss based stopping criterion in this section. Figure~\ref{fHinge-0-1} shows the retrieval results of both the video-level scoring function $f^{\textbf{V}}_\text{corr-2}$ and shot-level function $f^{\text{VS}}_{\text{latent}}$, respectively, with and without using the 0-1 loss in the optimization. We can see that coupling the 0-1 loss with the hinge loss in optimization significantly improves the performance of the hinge loss alone for both types of scoring functions. This is not surprising. The 0-1 loss is advantageous over the hinge loss especially when there are ``difficult'' pairs which heavily violate the ranking constraint in the training stage. The hinge loss would penalize more those pairs and consequently ignore the other pairs, but 0-1 loss is resilient to those pairs. Although in practice we cannot harvest the appealing modeling power of the 0-1 loss, our results in Figure~\ref{fHinge-0-1} verify that the nice properties of the 0-1 loss can be transferred indirectly by defining the new stopping criterion with the 0-1 loss.

%However, we have to also note that the solution of the 0-1 loss  is only approximately found by the sub-gradients of the hinge loss. As a result, the optimization process may discount the modeling power of the 0-1 loss. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{./images/Generalization2}
	\caption{The video retrieval results using \emph{previously unseen} queries: (a) single-concept queries, (b) pair-concept queries, and (c) triple-concept queries. }
	\label{fTriplets}
	%\vspace{-10pt}
\end{figure}

\subsection{Generalizing out of the training queries}
After training our models using the pair-concept queries, we expect it to also generalize well to other multi-concept queries. We choose three sets of new queries for this experiment: (a) 30 single-concept queries, (b) 50 new pair-concept queries, and (c) 50 new triple-concept queries. None of them are used to train our model. The 50 concept triplets are shown in Table~\ref{tQueries}. Figure~\ref{fTriplets} shows the retrieval results using different variations of our model. We can see our models with the shot-level scoring functions $f^{\text{S}}_\text{latent}$ and $f^{\text{VS}}_\text{latent}$ performs quite well upon the new queries. The results are not only significantly better than the simple average but also comparable to those on the seen pair-concept queries (cf.\ Table~\ref{tComparison}). The video-level scoring function $f^{\text{V}}_\text{corr-2}$ unfortunately degrades and gives similar or worse performance comparing to the averaging baseline $f^{\text{V}}_\text{avg}$.

In a complementary experiment, we show our model can be used on TRECVID SIN challenge. Eventhough this challenge is based on short video shots and signle concept queries retrieval, an improvement is expectedt to be observed because of concepts correlations that our model captures. For this experiment, we have the same testing pipeline. Shots have been considered as videos and each frame as a single shot. You can find the results on Table~\ref{tSIN}. We used the same settings as explained in Section 4.1.1 and the learned model is same as used in other experiments. To give an insight about how well the independent concept detectors are working, we try them on the full set as well. The numbers are reported using mean InfAP~\cite{yilmaz2006estimating} over 30 concepts. First 2000 retrieved shots for each concept has been considered. Note that our model is not applicable on full dataset due to 50\% usage of that in training stage.



\begin{table}
	\centering
	\caption{Results over TRECVID 2014 SIN challenge.}   \label{tSIN}
	%\begin{small}
	\begin{tabular}{cccccccccccccccccccc}
		\hline
		\multicolumn{5}{| c |}{Method} & 
		\multicolumn{5}{| c |}{$f^{\text{V}}_{\text{avg}}$} &
		\multicolumn{5}{| c |}{$f^{\text{VS}}_{\text{latent}}$} \\
		\hline
		\multicolumn{5}{| c |}{IACC.2.B (Full) - TRECVID SIN 2014} & 
		\multicolumn{5}{| c |}{$24.01$} & 
		\multicolumn{5}{| c |}{-}  \\
		
		\multicolumn{5}{| c |}{IACC.2.B (Half) - TRECVID SIN 2014} &
		\multicolumn{5}{| c |}{$24.56$} &
		\multicolumn{5}{| c |}{$24.80$}  \\
	
		
			
	
		\hline
		
	\end{tabular}
	%\end{small}
\end{table}


\begin{table}
	\centering
	\caption{Baseline averages of NDCG@5-50 on three different datasets}   \label{tNew}
	%\begin{small}
	\begin{tabular}{cccccccccccccccccccc}
		\hline
		\multicolumn{5}{| c |}{Dataset} & \multicolumn{5}{| c |}{IACC.2.B} &
		\multicolumn{5}{| c |}{IACC.2.C}&
		\multicolumn{5}{| c |}{IACC.2.B + C} \\
		\hline
		
		\multicolumn{5}{| c |}{TagProp} & 
		\multicolumn{5}{| c |}{$0.2888$} & 
		\multicolumn{5}{| c |}{$0.241$} &
		\multicolumn{5}{| c |}{$0.123$} \\
		
		
		\multicolumn{5}{| c |}{Rank-SVM} & 
		\multicolumn{5}{| c |}{$0.555$} & 
		\multicolumn{5}{| c |}{$0.264$} &
		\multicolumn{5}{| c |}{$0.395$} \\
		
		
		\multicolumn{5}{| c |}{Co-occurrence} & 
		\multicolumn{5}{| c |}{$0.538$} & 
		\multicolumn{5}{| c |}{$0.444$} &
		\multicolumn{5}{| c |}{$0.302$} \\
		
		\multicolumn{5}{| c |}{$f^{\text{V}}_{\text{avg}}$} & 
		\multicolumn{5}{| c |}{$0.593$} & 
		\multicolumn{5}{| c |}{$0.537$} &
		\multicolumn{5}{| c |}{$0.404$} \\
		
		\multicolumn{5}{| c |}{$f^{\text{V}}_{\text{corr-2}}$} & 
		\multicolumn{5}{| c |}{$0.607$} &
		\multicolumn{5}{| c |}{$0.501$} &
		\multicolumn{5}{| c |}{$0.378$} \\
		
		\multicolumn{5}{| c |}{$f^{\text{VS}}_{\text{latent}}$} & 
		\multicolumn{5}{| c |}{$\textbf{0.649}$} &
		\multicolumn{5}{| c |}{$\textbf{0.569}$} &
		\multicolumn{5}{| c |}{$\textbf{0.435}$} \\
		\hline
		
	\end{tabular}
	%\end{small}
\end{table}




\subsection{The IACC.2.C dataset}
We extended our experiments using IACC.2.C dataset by dividing it to training, validation and testing sets with similar settings as explained in Section 4.1.1. Also, by integrating it with IACC.2.B, we built one super dataset. Different experiments shown in  Table~\ref{tNew} are various methods and all of them had a drop, however, in all of three cases we see an improvement after applying our method.

We show the reason behind dropping baseline accuracies in Table~\ref{DiffYears}. By decreasing the ratio of positives to total number of samples in datasets, we see a drop in baseline performance. 50 queries with highest frequency of positives has been considered for each set.

\begin{table}
	\centering
	\caption{Mean ratio of positives to total number of videos for pair-concept queries}   \label{DiffYears}
	%\begin{small}
	\begin{tabular}{cccccccccccccccccccc}
		\hline
		\multicolumn{5}{| c |}{Dataset} &
		\multicolumn{5}{| c |}{IACC.2.B} &
		\multicolumn{5}{| c |}{IACC.2.C}&
		\multicolumn{5}{| c |}{IACC.2.B + C} \\
		\hline
		
		\multicolumn{5}{| c |}{Mean Ratio} &
		\multicolumn{5}{| c |}{$\textbf{0.0159}$} & 
		\multicolumn{5}{| c |}{$0.0134$} &
		\multicolumn{5}{| c |}{$0.0106$} \\
		\hline
		
	\end{tabular}
	%\end{small}
\end{table}



\subsection{User study on YouTube search engine}
We built a new dataset by 230 videos gathered from YouTube. These videos are retrieved using 9 of pair-concept queries showed in Table~\ref{tQueries}. 20 to 30 videos for each of queries have been downloaded and their actual rank in YouTube has been saved. Shots are also built by clipping videos each 2 seconds. We applied our method for each query and got a new ranking list. 10 random users have been invited to compare these two ranking lists. One of them was YouTube original list and the other was coming from our method, and we asked them to choose the better list for corresponding query search, just based on visual clues. Eventhough, our detectors are trained on training set explained in Section 4.1.3 and there is a domain difference between YouTube videos and that dataset, in Figure~\ref{YouTubeVote} we show for some queries, users are mostly like the re-ranked list using our method. YouTube ranking has a biased toward meta-data coming with the video and our method can bring up videos which can have the actual concepts visually happening in them.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{./images/YouTubeVote}
	\caption{Number of votes for YouTube list and our method.}
	\label{YouTubeVote}
	
\end{figure}




\subsection{Qualitative analyses}
We show some videos and their ranks using $f^{\text{V}}_{\text{avg}}$ and also after applying $f^{\text{VS}}_{\text{latent}}$  in Figure~\ref{fExamples}. In general, our approach works especially well under the following two scenarios. One is when the concepts appear in different video shots, and the other is  when the concepts are not very relevant (e.g., bridge, chair, and highway).  


\begin{figure*}
	\centering
	\includegraphics[width=1.0\linewidth]{./images/examplesVertical}
	\caption{Some examples of queries and their ranks. Each row is representing for one video and each column is corresponding to a sample frame of a shot.}
	\label{fExamples}
\end{figure*}


