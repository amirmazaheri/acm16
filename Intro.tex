\section{Introduction}
% !TEX root = main.tex

\label{sIntro}

\begin{figure*}
\centering
	\includegraphics[width=0.85\linewidth]{./images/Cover_Final_acm}
	\caption{How to calculate the ranking scores of video in response to one or more concepts,  is the central component in many video retrieval systems. It takes as input the multi-concept queries and then returns a ranked list of videos. The multiple concepts in a query could be directly supplied by the users or inferred by the systems from the users\rq{} text queries. } 
	\label{fConcept}
	\vspace{-12pt}
\end{figure*}



Video data is explosively growing from surveillance, health care, and  personal mobile phones to name a few sources. From the perspective of the IP traffic, Cisco\rq{}s white papers on Visual Networking Index reveal several astonishing numbers about the Internet videos~\cite{index520862global,index2013zettabyte}:
\begin{itemize} \setlength\itemsep{0.1pt}
	\item The sum of all forms of videos will be in the range of 80 to 90 percent of global consumer traffic by 2019.
	\item It would take an individual more than 5 million years to watch the amount of video that will cross global IP networks each month in 2019.
	\item Mobile video traffic exceeded 50 percent of total mobile data traffic in 2012. Note that the global mobile data traffic reached 2.5 exabytes ($2.5\times10^{18}$ bytes) per month  at the end of 2014.
	%\vspace{-16pt}
\end{itemize}
Indeed, there have been way more videos being generated than what people can watch. Some people have started making fun of this fact; the PetitTube (\textit{www.petittube.com}) randomly plays YouTube (\textit{www.youtube.com}) videos with zero views to its visitors, such that a visitor will become the first one to watch the randomly displayed video. However, it is not funny at all to see the huge volumes of unwatched videos, but rather alarming, for example in the public security domain, and rather challenging for  video providers to deliver the right videos upon consumers\rq{} requests. 

By all means, effective and efficient video retrieval has become a pressing need in the era of ``big video'', whereas it has been an active research area for decades. Following the earlier research on \emph{content} based video retrieval~\cite{aslandogan1999techniques,hu2011survey}, the most recent efforts have been mainly spent on \emph{(multi-)concept} based video retrieval~\cite{snoek2008concept}, an arguably more promising paradigm to bridge the semantic gap between the visual appearance in videos and the high-level interpretations humans perceive from the videos. 
We refer the readers to the survey papers~\cite{snoek2008concept,hu2011survey} and the annual  TRECVID workshops~\cite{over2014trecvid} for a more comprehensive understanding.  



A concept corresponds to one or more words or a short description that is understandable by humans. To be useful in automatic video retrieval systems, the concepts (e.g., furniture, beach, etc.) have also to be automatically detectable, usually by some statistical machine learning algorithms, from the low-level visual cues (color, texture, etc.) in the videos. Some studies have shown that a rich family of concepts coupled with even poor detection results (10\% mean average precision) is able to provide high accuracy results on news video retrieval---comparable to text retrieval on the Web~\cite{hauptmann2007many}. Both the richness of the concept set and the performance of the concept detectors are essential. Correspondingly, a plethora of works has been devoted to developing concept detectors~\cite{chang2007large,qi2007correlative,yang2012complex,snoek2013mediamill,dehghanucf,ye2015eventnet}.


Common users have been used to text-based queries for retrieving the target instances in their minds. Equipped with a set of concept detectors, a concept-based video retrieval system is able to accept text descriptions as the queries even when the videos have no textual meta data associated, thus offering users the same interface for the video retrieval as for document or website retrieval (e.g.,~\cite{snoek2007adding}). See Figure~\ref{fConcept}. Users can directly select concepts from a checklist to compose the queries. Alternatively, the system can also let the users to query using open-vocabulary texts and then translate the queries to a subset of concepts. The latter itself is a very interesting problem to which a full treatment is beyond the scope of this paper. Readers who are interested in this problem are referred to~\cite{neo2006video,wang2007importance,natsev2007semantic,snoek2007adding,haubold2008web,wei2008selection,kennedy2008query}. 
In either case, the central operation afterwards is to use the resultant subset of concepts to retrieve related videos. In this paper, we focus on retrieving whole videos as opposed to segments or shots of videos; however, the developed approach can be conveniently applied to video segments retrieval as well. 

Despite being the key component in (multi-)concept based video retrieval, how to effectively retrieve videos that are related to a subset of concepts is left far from being solved. There is a lack of principled framework or unified statistical machine learning model for this purpose. Instead, most existing works take the easy alternative by ranking videos according to the weighted average of the concept detection confidences~\cite{mc2005comparison,aytar2008utilizing}, where the weights are either uniform or in some systems derived from the similarities between an open-vocabulary user query and the selected concepts. This necessarily fails to take account of the reliabilities of the concept detectors, the relationships between the selected concepts, and the potential contributions from the unselected concepts. Our empirical study actually shows that the unselected concepts can significantly boost the performance when they are modeled appropriately. 

%Videos can be very dynamic. Concepts happen or disappear in different parts of a video. It is too expensive and impractical to have fully annotated videos. As a result, the common way to annotate a video is to mark a few parts of it, as positive or negative for a particular concept and leave the rest as unjudged. On the other hand, it is trivial to revisit same concept in a video with different appearances. In this approach, we use all of a video not just the annotated parts, however, using a latent variable, we can supress the effect of misleading untagged shots of a video and leverage from benefitial parts in training.

The objective of this work is to provide a principled model for multi-concept based video retrieval, where  the concepts could be directly provided by the users or automatically selected by the systems based on user queries. 

%Despite of other works, this method trains on multiple-concepts queries to leverage from behavior of detectors when happening with other ones. It can help to supress the score of weak detectors and give more value to better ones. In this way, we supress the noise of bad detectors which are very harmful in case of simple averaging baseline for multi-concept video retrieval.

Our approach, which can be considered as a latent ranking SVM~\cite{lan2012image}, integrates different advantages of the recent works on text and multi-attribute based image retrieval. Particularly, we model the video retrieval as a ranking problem following~\cite{grangier2008discriminative}, as opposed to the structured prediction problem used in~\cite{siddiquie2011image,yu2012weak}, in order to harvest better efficiency and larger modeling capacity to accommodate a latent variable. The latent variable is used for us to define the scoring functions both within and across the video shots, closely tracking the unique temporal characteristic of videos. Besides, we incorporate the empirically successful intuitions from~\cite{siddiquie2011image,yu2012weak} that the inter-dependencies between both selected and unselected concepts should be jointly modeled. Finally, we introduce a novel 0-1 loss based early stopping criterion for learning/optimizing our model parameters. This is motivated by the fact that the 0-1 loss is more robust to the outliers than  the hinge loss, which is used to formalize the optimization problem. 

The proposed model, along with a family of scoring functions, 
accounts for some inevitable caveats of the concept detection results: reliabilities of individual concept detectors, inter-dependencies between concepts, and the correlations between selected and unselected concepts in response to a user query. It expressively improves upon the conventional weighted average of the selected concepts' detection scores for the multi-concept based video retrieval. To this end, our secondary objective is to stress again that, as the central component in modern video retrieval systems, how to effectively transform the selected concepts to the ranking scores of videos has been long overlooked and is  under exploited. More advances and progresses on this problem are in need since they will significantly increase the overall performance of the  video retrieval systems.


In the following, we first discuss the related work in Section~\ref{sRelated}. Section~\ref{sModel} presents our video retrieval model given multiple concepts. We then describe how to integrate video-level concept detectors (Section~\ref{sVideo}) and video shot-level detectors (Section~\ref{sShot}) into the model, followed by experiments in Section~\ref{sExp}.




